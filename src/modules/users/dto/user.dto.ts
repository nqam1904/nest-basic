import { BaseResponse } from '@common';

export class UserDto {
  fullname: string;

  username: string;

  email: string;

  password: string;

  phone: string;

  createdAt: Date;

  updatedAt: Date;
}
export class UserResponse extends BaseResponse<UserDto> {}
